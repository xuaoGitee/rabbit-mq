package com.xa.bean;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Msg implements Serializable {
      private String  messageId;
      private String  subject;
      private String  messageData;
      private String  createTime;
      private String  email;
}
