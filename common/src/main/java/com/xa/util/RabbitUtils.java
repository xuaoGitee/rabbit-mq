package com.xa.util;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author xuao
 * version 1.0
 */
public class RabbitUtils {
    public static Channel getChannel() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        //工厂ip
        factory.setHost("47.100.220.41");
        //设置端口
        factory.setPort(5672);
        //用户名
        factory.setUsername("admin");
        //密码
        factory.setPassword("123");
        //创建链接
        Connection connection = factory.newConnection();
        //获取信道
        return connection.createChannel();
    }
}
