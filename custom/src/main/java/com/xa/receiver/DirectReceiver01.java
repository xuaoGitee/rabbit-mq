package com.xa.receiver;

import com.xa.bean.Msg;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
//@RabbitListener(queues = "TestDirectQueue",containerFactory="rabbitListenerContainerFactory")//监听的队列名称 TestDirectQueue
public class DirectReceiver01 {

    private final JavaMailSenderImpl mailSender;
    public DirectReceiver01(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }
    @Value("${spring.mail.username}")
    private String emailFrom;

    @RabbitHandler
    @RabbitListener(queues = "TestDirectQueue",containerFactory="rabbitListenerContainerFactory")//监听的队列名称 TestDirectQueue
    public void process(Msg msg) {
        System.out.println("DirectReceiver01 消费者收到消息 : " + msg.toString());
        //简单邮件
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(emailFrom);
        simpleMailMessage.setTo(msg.getEmail());
        simpleMailMessage.setSubject(msg.getSubject());
        simpleMailMessage.setText(msg.getMessageData());
        mailSender.send(simpleMailMessage);

    }
}